import cv2
import numpy as np
from matplotlib import pyplot as plt
import random
#import Homograpy_functions ## 

ill=0
yaz=['A','B','C','D']
koordinat_o=[]
koordinat_p=[]
 
    
if __name__ == '__main__' :
    global im_src,im_dst
    capt=0 # capt=0 captures from file and capt= 1 captures from camera port 2 ##    
    im_src=resim_al('r1.jpg',15)    
    if capt==0 :im_dst=resim_al('r6.jpg',15)
    else :im_dst=capture(2)
# resimlerin dönüşlerini tespit etmek için üzerlerine harf yazmıştım
#    cv2.imshow('im_dst',im_dst)              
#    cv2.setMouseCallback('im_dst', chose_harf)
#    cv2.waitKey(0)    
    
    print('Orginal seçilen koseler ::') 
    koseler('orginal_kose',im_src,'o')   # for selecting for points from orginal image
    print()
    print('Captured seçilen koseler ::')

    koseler('process_kose',im_dst,'p') # for selecting for points from test image
    print()
    print('Koordinatlar Org ::')
    print()   
    print(koordinat_o)    
    print('Koordinatlar Captured ::')

    print(koordinat_p) 
    ik=0
    #harf_bas('im_dst',im_dst)

    print()     
    # these for rows are for stacking all points in one matris
    pts_dst= np.array([koordinat_o[0], koordinat_o[1], koordinat_o[2],koordinat_o[3]]) 
    pts_src= np.array([koordinat_p[0], koordinat_p[1], koordinat_p[2],koordinat_p[3]])
 
    pts1 = np.float32([koordinat_o[0], koordinat_o[1], koordinat_o[2],koordinat_o[3]])
    pts2 = np.float32([koordinat_p[0], koordinat_p[1], koordinat_p[2],koordinat_p[3]])
    
    if 1==1:
        H=manuel_homography(pts_src,pts_dst)     # manuel calculated Homograpy
        H1 = cv2.getPerspectiveTransform(pts2,pts1) # getperspective function  calculated Homograpy
        print('getperspective transform ::')
        print(H1)
        print()
        print('Manuel Homography ::')
        print(H)
        print()

        h, status = cv2.findHomography(pts_src, pts_dst) # opencv findhomograpy function  calculated Homograpy
        print('Open Cv Homography ::')
        print(h)
        print()    
        h_subpx=h_cv_subpxl(im_src,im_dst)    ## opencv analysis all picture to find common points
    #    h_vc,s=cv2.findHomography(im_src, im_dst, cv2.RANSAC,5.0)    
    
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(im_dst, h, (im_dst.shape[1],im_dst.shape[0]))
    im_out_M = cv2.warpPerspective(im_dst, H, (im_dst.shape[1],im_dst.shape[0]))
    #im_out_CV = cv2.warpPerspective(im_dst, h_cv, (im_dst.shape[1],im_dst.shape[0]))
    im_out_sub = cv2.warpPerspective(im_dst, h_subpx, (im_dst.shape[1],im_dst.shape[0]))
    
    cv2.imshow("Orginal Image", im_src)
    cv2.imshow("Captured Frame", im_dst)
    cv2.imshow("Processed Image FindHomograhy", im_out)
    cv2.imshow("Processed Image Manuel", im_out_M)
    #cv2.imshow("Processed Image Subpx_low open cv", im_out_CV)
    cv2.imshow("Processed Image Subpx_BEST  open cv", im_out_sub)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
